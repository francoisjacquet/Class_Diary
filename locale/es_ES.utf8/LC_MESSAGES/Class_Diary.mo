��    
      l      �       �      �      �             
        $  &   )  *   P     {  �  �     g     x     �     �     �     �  &   �      �     �            
                	                 Class Diaries Class Diary Entries Entry Last Entry Read The entry has been added to the diary. The entry has been removed from the diary. Write Project-Id-Version: Class Diary module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 17:36+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Diarios de Clase Diario de Clase Entradas Entrada Última Entrada Leer La entrada ha sido añadida al diario. Se quitó la entrada del diario. Escribir 