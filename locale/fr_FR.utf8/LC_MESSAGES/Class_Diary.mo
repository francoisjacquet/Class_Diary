��    
      l      �       �      �      �             
        $  &   )  *   P     {  �  �     c     v     �     �     �     �  &   �  &   �     �            
                	                 Class Diaries Class Diary Entries Entry Last Entry Read The entry has been added to the diary. The entry has been removed from the diary. Write Project-Id-Version: Class Diary module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 17:36+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Journaux de classe Journal de classe Entrées Entrée Dernière entrée Lire L'entrée a été ajoutée au journal. L'entrée a été retirée du journal. Écrire 