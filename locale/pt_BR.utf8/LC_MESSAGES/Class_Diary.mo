��    
      l      �       �      �      �             
        $  &   )  *   P     {  �  �     M     `     r     {     �     �  $   �  "   �     �            
                	                 Class Diaries Class Diary Entries Entry Last Entry Read The entry has been added to the diary. The entry has been removed from the diary. Write Project-Id-Version: Class Diary module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 17:37+0200
Last-Translator: Emerson Barros
Language-Team: RosarioSIS <info@rosariosis.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Diários de classe Diário de classe Entradas Entrada Última entrada Ler A entrada foi adicionada ao diário. A entrada foi removida do diário. Escrever 